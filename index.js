// console.log("Hello world");



//-------------------------------------- 3. 


const getCube = 2 ** 3;




//-------------------------------------- 4. 



value = `The cube of 2 is ${getCube}`;
console.log(value);



//-------------------------------------- 5. 




const printAddress = ["258 Washington Ave", "NW", "California", 90011];




//-------------------------------------- 6. 



const [street, city, state, zipCode] = printAddress;
console.log(`I live at ${street} ${city}, ${state} ${zipCode}`);






//-------------------------------------- 7. 



const animal = {
	type: "saltwater",
	kind: "crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in."
}



//-------------------------------------- 8. 



const {type, kind, weight, measurement} = animal;
console.log(`Lolong was a ${type} ${kind}. He weighed at ${weight} with a measurement of ${measurement}`);




//-------------------------------------- 9. 



const numbers = [1, 2, 3, 4, 5];




//-------------------------------------- 10. 


numbers.forEach((number) => {
	console.log(`${number}`);
})




//-------------------------------------- 11. 



const reduceNumber = [1, 2, 3, 4, 5];

let sum = reduceNumber.reduce((a, b) => {
	return a + b;
});

console.log(sum);




//-------------------------------------- 12. 



class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}






//-------------------------------------- 13. 


/*

ANOTHER WAY WITH valueName:

const myDog = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myDog);


*/



// USING DOT NOTATION

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";
console.log(myDog);












